package com.revolut.test.task.trasfers.repository;

import com.revolut.test.task.trasfers.repository.DatabaseConnectionContextHolder.Configuration;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class DatabaseConnectionContextHolderTest {

    @Test
    public void contextInitialized() {
        DatabaseConnectionContextHolder databaseConnectionContextHolder = DatabaseConnectionContextHolder.init("application.properties");
        DSLContext context = databaseConnectionContextHolder.getContext();
        assertThat(context, is(notNullValue()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void configurationIsNotSetConfiguration() {
        DatabaseConnectionContextHolder.init("fake.properties");
    }

    @Test
    public void configurationIsSetConfiguration() {
        DatabaseConnectionContextHolder databaseConnectionContextHolder = DatabaseConnectionContextHolder.init("application.properties");
        Configuration configuration = databaseConnectionContextHolder.getConnectionConfiguration();
        assertThat(configuration, is(notNullValue()));
    }
}
