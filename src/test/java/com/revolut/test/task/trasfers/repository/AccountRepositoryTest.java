package com.revolut.test.task.trasfers.repository;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.revolut.test.task.trasfers.TransferModule;
import com.revolut.test.task.trasfers.beans.Account;
import com.revolut.test.task.trasfers.support.DatabaseMigrationsExecutor;
import net.lamberto.junit.GuiceJUnitRunner;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

import static com.revolut.test.task.trasfers.helper.TestDataHelper.clearTestData;
import static com.revolut.test.task.trasfers.helper.TestDataHelper.initTestData;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

@RunWith(GuiceJUnitRunner.class)
@GuiceJUnitRunner.GuiceModules(TransferModule.class)
public class AccountRepositoryTest {

    private static final String NOT_FOUND_ACCOUNT = "2222 2222 2222 2222";
    private static final String EXIST_ACCOUNT = "1234 1234 1234 1234";

    private static DatabaseConnectionContextHolder databaseConnectionContextHolder;
    private static DatabaseMigrationsExecutor databaseMigrationsExecutor;

    @Inject
    private AccountRepository accountRepository;


    @BeforeClass
    public static void setUpClass() {
        Injector injector = Guice.createInjector(new TransferModule());
        databaseConnectionContextHolder = injector.getInstance(DatabaseConnectionContextHolder.class);
        databaseMigrationsExecutor = injector.getInstance(DatabaseMigrationsExecutor.class);
        databaseMigrationsExecutor.initOrUpdateSchema();
    }

    @Before
    public void setUp() {
        clearTestData(databaseConnectionContextHolder);
        initTestData(databaseConnectionContextHolder);
    }

    @AfterClass
    public static void tearDownClass() {
        databaseMigrationsExecutor.clearSchema();
    }

    @Test
    public void findByAccountShouldReturnNullForNullAccountId() {
        Account account = accountRepository.findByAccount(databaseConnectionContextHolder.getContext(), null);
        assertThat(account, is(nullValue()));
    }

    @Test
    public void findByAccountShouldReturnNullForNotExistedAccount() {
        Account account = accountRepository.findByAccount(databaseConnectionContextHolder.getContext(), NOT_FOUND_ACCOUNT);
        assertThat(account, is(nullValue()));
    }

    @Test
    public void findByAccountShouldReturnValue() {
        Account account = accountRepository.findByAccount(databaseConnectionContextHolder.getContext(), EXIST_ACCOUNT);
        // TODO: Account is null.
        assertThat(account, is(notNullValue()));
        assertThat(account.getAccount(), is(EXIST_ACCOUNT));
    }

    @Test
    public void updateAccountAmount() {
        double plusAmount = 100D;
        Account account = accountRepository.findByAccount(databaseConnectionContextHolder.getContext(), EXIST_ACCOUNT);

        accountRepository.updateAmount(databaseConnectionContextHolder.getContext(), account.getUuid(), account.getTotalAmount().add(BigDecimal.valueOf(plusAmount)));
        Account updated = accountRepository.findByAccount(databaseConnectionContextHolder.getContext(), EXIST_ACCOUNT);

        assertThat(updated, is(notNullValue()));
        assertThat(updated.getAccount(), is(EXIST_ACCOUNT));
        assertThat(updated.getTotalAmount(), is(account.getTotalAmount().add(BigDecimal.valueOf(plusAmount))));
    }
}
