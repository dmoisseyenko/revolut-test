package com.revolut.test.task.trasfers.api;

import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.revolut.test.task.tables.tables.AccountTransactions;
import com.revolut.test.task.trasfers.TransferModule;
import com.revolut.test.task.trasfers.beans.Account;
import com.revolut.test.task.trasfers.beans.AccountTransactionStatus;
import com.revolut.test.task.trasfers.beans.TransferStatus;
import com.revolut.test.task.trasfers.dto.TransferDto;
import com.revolut.test.task.trasfers.dto.TransferResponseDto;
import com.revolut.test.task.trasfers.helper.RestApiTestHelper;
import com.revolut.test.task.trasfers.helper.RestApiTestHelper.TestResponse;
import com.revolut.test.task.trasfers.repository.AccountRepository;
import com.revolut.test.task.trasfers.repository.DatabaseConnectionContextHolder;
import com.revolut.test.task.trasfers.support.DatabaseMigrationsExecutor;
import net.lamberto.junit.GuiceJUnitRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import spark.Spark;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static com.revolut.test.task.trasfers.api.TransferMoneyRestApi.TRANSFER_MONEY_ROUTE;
import static com.revolut.test.task.trasfers.helper.TestDataHelper.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(GuiceJUnitRunner.class)
@GuiceJUnitRunner.GuiceModules(TransferModule.class)
public class TransferMoneyRestApiTest {

    private static final String POST = "POST";
    private static final int BAD_REQUEST_CODE = 400;

    private static RestApiTestHelper restApiTestHelper;
    private static DatabaseMigrationsExecutor databaseMigrationsExecutor;
    private static DatabaseConnectionContextHolder databaseConnectionContextHolder;

    @Inject
    private AccountRepository accountRepository;

    @BeforeClass
    public static void setUpClass() {
        Injector injector = Guice.createInjector(new TransferModule());

        databaseConnectionContextHolder = injector.getInstance(DatabaseConnectionContextHolder.class);
        databaseMigrationsExecutor = injector.getInstance(DatabaseMigrationsExecutor.class);
        databaseMigrationsExecutor.initOrUpdateSchema();

        injector.getInstance(TransferMoneyRestApi.class).initRoutes();
        Spark.awaitInitialization();
        restApiTestHelper = new RestApiTestHelper("localhost", Spark.port());
    }

    @AfterClass
    public static void tearDownClass() {
        databaseMigrationsExecutor.clearSchema();
        Spark.stop();
    }

    @Before
    public void setUp() {
        clearTestData(databaseConnectionContextHolder);
        initTestData(databaseConnectionContextHolder);
    }

    @Test
    public void transferMoney_failed_senderNotFount() throws Exception {
        TransferDto dto = TransferDto.builder()
                .sender(NOT_FOUND_SENDER_ACCOUNT_NUMBER)
                .receiver(RECEIVER_ACCOUNT_NUMBER)
                .amount(100D)
                .build();

        TestResponse<TransferResponseDto> response = restApiTestHelper.execute(POST, TRANSFER_MONEY_ROUTE, toJson(dto), TransferResponseDto.class);

        assertThat(response.getCode(), is(BAD_REQUEST_CODE));
        assertThat(response.getBody().getStatus(), is(TransferStatus.SENDER_NOT_FOUND));
    }

    @Test
    public void transferMoney_failed_receiverNotFount() throws Exception {
        TransferDto dto = TransferDto.builder()
                .sender(SENDER_ACCOUNT_NUMBER)
                .receiver(NOT_FOUND_RECEIVER_ACCOUNT_NUMBER)
                .amount(100D)
                .build();

        TestResponse<TransferResponseDto> response = restApiTestHelper.execute(POST, TRANSFER_MONEY_ROUTE, toJson(dto), TransferResponseDto.class);

        assertThat(response.getCode(), is(BAD_REQUEST_CODE));
        assertThat(response.getBody().getStatus(), is(TransferStatus.RECEIVER_NOT_FOUND));
    }

    @Test
    public void transferMoney_failed_notEnoughMoney() throws Exception {
        TransferDto dto = TransferDto.builder()
                .sender(SENDER_ACCOUNT_NUMBER)
                .receiver(RECEIVER_ACCOUNT_NUMBER)
                .amount(10000D)
                .build();

        TestResponse<TransferResponseDto> response = restApiTestHelper.execute(POST, TRANSFER_MONEY_ROUTE, toJson(dto), TransferResponseDto.class);

        assertThat(response.getCode(), is(BAD_REQUEST_CODE));
        assertThat(response.getBody().getStatus(), is(TransferStatus.NOT_ENOUGH_MONEY));
    }

    @Test
    public void transferMoney_failed_sameTransferAccount() throws Exception {
        TransferDto dto = TransferDto.builder()
                .sender(SENDER_ACCOUNT_NUMBER)
                .receiver(SENDER_ACCOUNT_NUMBER)
                .amount(10000D)
                .build();

        TestResponse<TransferResponseDto> response = restApiTestHelper.execute(POST, TRANSFER_MONEY_ROUTE, toJson(dto), TransferResponseDto.class);

        assertThat(response.getCode(), is(BAD_REQUEST_CODE));
        assertThat(response.getBody().getStatus(), is(TransferStatus.SAME_TRANSFER_ACCOUNT));
    }

    @Test
    public void transferMoney_failed_negativeAmount() throws Exception {
        TransferDto dto = TransferDto.builder()
                .sender(SENDER_ACCOUNT_NUMBER)
                .receiver(RECEIVER_ACCOUNT_NUMBER)
                .amount(-10000D)
                .build();

        TestResponse<TransferResponseDto> response = restApiTestHelper.execute(POST, TRANSFER_MONEY_ROUTE, toJson(dto), TransferResponseDto.class);

        assertThat(response.getCode(), is(BAD_REQUEST_CODE));
        assertThat(response.getBody().getStatus(), is(TransferStatus.NEGATIVE_OR_ZERO_AMOUNT));
    }

    @Test
    public void transferMoney_failed_zeroAmount() throws Exception {
        TransferDto dto = TransferDto.builder()
                .sender(SENDER_ACCOUNT_NUMBER)
                .receiver(RECEIVER_ACCOUNT_NUMBER)
                .amount(0D)
                .build();

        TestResponse<TransferResponseDto> response = restApiTestHelper.execute(POST, TRANSFER_MONEY_ROUTE, toJson(dto), TransferResponseDto.class);

        assertThat(response.getCode(), is(BAD_REQUEST_CODE));
        assertThat(response.getBody().getStatus(), is(TransferStatus.NEGATIVE_OR_ZERO_AMOUNT));
    }

    @Test
    public void transferMoney_status_successful() throws Exception {
        double transferAmount = 99.99;
        TransferDto dto = TransferDto.builder()
                .sender(SENDER_ACCOUNT_NUMBER)
                .receiver(RECEIVER_ACCOUNT_NUMBER)
                .amount(transferAmount)
                .build();
        TestResponse<TransferResponseDto> response = restApiTestHelper.execute(POST, TRANSFER_MONEY_ROUTE, toJson(dto), TransferResponseDto.class);

        assertThat(response.getCode(), is(200));
        assertThat(response.getBody().getStatus(), is(TransferStatus.SUCCESSFUL));
    }

    @Test
    public void transferMoney_fromSenderToReceiver_successful() throws Exception {
        double transferAmount = 99.99;
        BigDecimal senderTotalAmountBefore = accountRepository.findByAccount(databaseConnectionContextHolder.getContext(), SENDER_ACCOUNT_NUMBER).getTotalAmount();
        BigDecimal receiverTotalAmountBefore = accountRepository.findByAccount(databaseConnectionContextHolder.getContext(), RECEIVER_ACCOUNT_NUMBER).getTotalAmount();
        TransferDto dto = TransferDto.builder()
                .sender(SENDER_ACCOUNT_NUMBER)
                .receiver(RECEIVER_ACCOUNT_NUMBER)
                .amount(transferAmount)
                .build();

        TestResponse<TransferResponseDto> response = restApiTestHelper.execute(POST, TRANSFER_MONEY_ROUTE, toJson(dto), TransferResponseDto.class);
        awaitStatusChange(response);
        Account senderAfterTransfer = accountRepository.findByAccount(databaseConnectionContextHolder.getContext(), SENDER_ACCOUNT_NUMBER);
        Account receiverAfterTransfer = accountRepository.findByAccount(databaseConnectionContextHolder.getContext(), RECEIVER_ACCOUNT_NUMBER);

        assertThat(senderAfterTransfer.getTotalAmount(), is(senderTotalAmountBefore.subtract(BigDecimal.valueOf(transferAmount))));
        assertThat(receiverAfterTransfer.getTotalAmount(), is(receiverTotalAmountBefore.add(BigDecimal.valueOf(transferAmount))));
    }

    private void awaitStatusChange(TestResponse<TransferResponseDto> response) {
        boolean statusChanged = false;
        LocalDateTime expireWaitDate = LocalDateTime.now().plusMinutes(1);
        while (!statusChanged) {
            statusChanged = !AccountTransactionStatus.BLOCKED.name().equals(
                    databaseConnectionContextHolder.getContext()
                            .select(AccountTransactions.ACCOUNT_TRANSACTIONS.TRANSACTION_STATUS)
                            .from(AccountTransactions.ACCOUNT_TRANSACTIONS)
                            .where(AccountTransactions.ACCOUNT_TRANSACTIONS.UUID.eq(response.getBody().getTransactionUUID()))
                            .fetchOne()
                            .get(AccountTransactions.ACCOUNT_TRANSACTIONS.TRANSACTION_STATUS));
            if (expireWaitDate.isBefore(LocalDateTime.now())) {
                assertTrue("Test timed out", statusChanged);
            }
        }
    }

    private String toJson(TransferDto dto) {
        return new Gson().toJson(dto);
    }
}
