package com.revolut.test.task.trasfers.helper;

import com.google.gson.Gson;
import spark.utils.IOUtils;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static java.nio.charset.StandardCharsets.UTF_8;

public final class RestApiTestHelper {
    private final String host;
    private final int port;

    public RestApiTestHelper(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public <T> TestResponse<T> execute(String method, String route, String reqBody, Class<T> resultClass) throws Exception {
        URL url = new URL("http://" + host + ":" + port + "" + route);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            connection.setRequestMethod(method);
            connection.setDoOutput(true);
            if (reqBody != null) {
                byte[] outputInBytes = reqBody.getBytes(UTF_8);
                OutputStream os = connection.getOutputStream();
                os.write(outputInBytes);
                os.close();
            }
            connection.connect();
            String body = IOUtils.toString(connection.getInputStream());
            return new TestResponse<>(connection.getResponseCode(), new Gson().fromJson(body, resultClass));
        } catch (Exception ex) {
            String body = IOUtils.toString(connection.getErrorStream());
            return new TestResponse<>(connection.getResponseCode(), new Gson().fromJson(body, resultClass));
        }
    }

    public static class TestResponse<T> {
        private final int code;
        private final T body;

        TestResponse(int code, T body) {
            this.code = code;
            this.body = body;
        }

        public int getCode() {
            return code;
        }

        public T getBody() {
            return body;
        }
    }
}
