package com.revolut.test.task.trasfers.helper;

import com.revolut.test.task.tables.tables.Accounts;
import com.revolut.test.task.trasfers.beans.Account;
import com.revolut.test.task.trasfers.repository.DatabaseConnectionContextHolder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static com.revolut.test.task.tables.tables.AccountTransactions.ACCOUNT_TRANSACTIONS;
import static com.revolut.test.task.tables.tables.Accounts.ACCOUNTS;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class TestDataHelper {

    public static final String SENDER_ACCOUNT_NUMBER = "1234 1234 1234 1234";
    public static final String RECEIVER_ACCOUNT_NUMBER = "3333 3333 3333 3333";
    public static final String NOT_FOUND_SENDER_ACCOUNT_NUMBER = "1111 1111 1111 1111";
    public static final String NOT_FOUND_RECEIVER_ACCOUNT_NUMBER = "2222 2222 2222 2222";

    public static void initTestData(DatabaseConnectionContextHolder databaseConnectionContextHolder) {
        databaseConnectionContextHolder.getContext().transaction(ctx ->
                accounts().forEach(account -> ctx.dsl()
                        .insertInto(ACCOUNTS)
                        .set(Accounts.ACCOUNTS.UUID, account.getUuid())
                        .set(Accounts.ACCOUNTS.ACCOUNT, account.getAccount())
                        .set(Accounts.ACCOUNTS.TOTAL_AMOUNT, account.getTotalAmount())
                        .execute())
        );
    }

    public static void clearTestData(DatabaseConnectionContextHolder databaseConnectionContextHolder) {
        databaseConnectionContextHolder.getContext().delete(ACCOUNT_TRANSACTIONS).execute();
        databaseConnectionContextHolder.getContext().delete(ACCOUNTS).execute();
    }

    private static List<Account> accounts() {
        return Arrays.asList(
                createAccount(SENDER_ACCOUNT_NUMBER, BigDecimal.valueOf(100)),
                createAccount(RECEIVER_ACCOUNT_NUMBER, BigDecimal.valueOf(0.5D))
        );
    }

    private static Account createAccount(String account, BigDecimal amount) {
        return Account.builder()
                .uuid(UUID.randomUUID().toString())
                .account(account)
                .totalAmount(amount)
                .build();
    }
}
