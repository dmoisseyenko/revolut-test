package com.revolut.test.task.trasfers.service;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import com.revolut.test.task.trasfers.TransferModule;
import com.revolut.test.task.trasfers.beans.Account;
import com.revolut.test.task.trasfers.beans.TransferOperation;
import com.revolut.test.task.trasfers.beans.TransferStatus;
import com.revolut.test.task.trasfers.exception.*;
import com.revolut.test.task.trasfers.repository.AccountRepository;
import com.revolut.test.task.trasfers.repository.AccountTransactionRepository;
import com.revolut.test.task.trasfers.repository.DatabaseConnectionContextHolder;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;
import org.jooq.tools.jdbc.MockConnection;
import org.jooq.tools.jdbc.MockResult;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.UUID;

import static com.revolut.test.task.trasfers.helper.TestDataHelper.RECEIVER_ACCOUNT_NUMBER;
import static com.revolut.test.task.trasfers.helper.TestDataHelper.SENDER_ACCOUNT_NUMBER;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class TransferMoneyServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private AccountTransactionRepository accountTransactionRepository;
    @Mock
    private DatabaseConnectionContextHolder contextHolder;

    @Inject
    private TransferMoneyServiceImpl transferMoneyService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(contextHolder.getContext()).thenReturn(DSL.using(new MockConnection(mockExecuteContext -> new MockResult[0])));
        Guice.createInjector(Modules.override(new TransferModule()).with(new AbstractModule() {
            @Override
            protected void configure() {
                bind(AccountRepository.class).toInstance(accountRepository);
                bind(AccountTransactionRepository.class).toInstance(accountTransactionRepository);
                bind(DatabaseConnectionContextHolder.class).toInstance(contextHolder);
            }
        })).injectMembers(this);

        transferMoneyService = new TransferMoneyServiceImpl(accountRepository, contextHolder, accountTransactionRepository);
    }

    @Test(expected = SenderNotFoundException.class)
    public void transferMoney_shouldThrow_senderAccountNotFound() {
        transferMoneyService.transfer(TransferOperation.of(SENDER_ACCOUNT_NUMBER, RECEIVER_ACCOUNT_NUMBER, BigDecimal.valueOf(100)));
    }

    @Test(expected = SameTransferAccountException.class)
    public void transferMoney_shouldThrow_sameAccountTransfer() {
        transferMoneyService.transfer(TransferOperation.of(SENDER_ACCOUNT_NUMBER, SENDER_ACCOUNT_NUMBER, BigDecimal.valueOf(100.11)));
    }

    @Test(expected = ReceiverNotFoundException.class)
    public void transferMoney_shouldThrow_receiverAccountNotFound() {
        when(accountRepository.findByAccount(any(DSLContext.class), eq(SENDER_ACCOUNT_NUMBER)))
                .thenReturn(Account.builder()
                        .uuid(UUID.randomUUID().toString())
                        .account(SENDER_ACCOUNT_NUMBER)
                        .totalAmount(BigDecimal.valueOf(10))
                        .availableAmount(BigDecimal.valueOf(10))
                        .build());

        transferMoneyService.transfer(TransferOperation.of(SENDER_ACCOUNT_NUMBER, RECEIVER_ACCOUNT_NUMBER, BigDecimal.valueOf(100)));
    }

    @Test(expected = NotEnoughMoneyException.class)
    public void transferMoney_shouldThrow_notEnoughMoney() {
        when(accountRepository.findByAccount(any(DSLContext.class), eq(SENDER_ACCOUNT_NUMBER)))
                .thenReturn(Account.builder()
                        .uuid(UUID.randomUUID().toString())
                        .account(SENDER_ACCOUNT_NUMBER)
                        .totalAmount(BigDecimal.valueOf(100))
                        .availableAmount(BigDecimal.valueOf(7))
                        .build());
        when(accountRepository.findByAccount(any(DSLContext.class), eq(RECEIVER_ACCOUNT_NUMBER)))
                .thenReturn(Account.builder()
                        .uuid(UUID.randomUUID().toString())
                        .account(RECEIVER_ACCOUNT_NUMBER)
                        .totalAmount(BigDecimal.valueOf(10))
                        .build());

        transferMoneyService.transfer(TransferOperation.of(SENDER_ACCOUNT_NUMBER, RECEIVER_ACCOUNT_NUMBER, BigDecimal.valueOf(100)));
    }

    @Test(expected = NegativeOrZeroAmountException.class)
    public void transferMoney_shouldThrow_negativeAmount() {
        when(accountRepository.findByAccount(any(DSLContext.class), eq(SENDER_ACCOUNT_NUMBER)))
                .thenReturn(Account.builder()
                        .uuid(UUID.randomUUID().toString())
                        .account(SENDER_ACCOUNT_NUMBER)
                        .totalAmount(BigDecimal.valueOf(10))
                        .build());
        when(accountRepository.findByAccount(any(DSLContext.class), eq(RECEIVER_ACCOUNT_NUMBER)))
                .thenReturn(Account.builder()
                        .uuid(UUID.randomUUID().toString())
                        .account(RECEIVER_ACCOUNT_NUMBER)
                        .totalAmount(BigDecimal.valueOf(10))
                        .build());

        transferMoneyService.transfer(TransferOperation.of(SENDER_ACCOUNT_NUMBER, RECEIVER_ACCOUNT_NUMBER, BigDecimal.valueOf(-100)));
    }

    @Test
    public void transferMoney_saved_successfully() {
        String senderUUID = UUID.randomUUID().toString();
        String receiverUUID = UUID.randomUUID().toString();
        String returnedTransactionUUID = UUID.randomUUID().toString();
        when(accountRepository.findByAccount(any(DSLContext.class), eq(SENDER_ACCOUNT_NUMBER)))
                .thenReturn(Account.builder()
                        .uuid(senderUUID)
                        .account(SENDER_ACCOUNT_NUMBER)
                        .totalAmount(BigDecimal.valueOf(1000))
                        .availableAmount(BigDecimal.valueOf(1000))
                        .build());
        when(accountRepository.findByAccount(any(DSLContext.class), eq(RECEIVER_ACCOUNT_NUMBER)))
                .thenReturn(Account.builder()
                        .uuid(receiverUUID)
                        .account(RECEIVER_ACCOUNT_NUMBER)
                        .totalAmount(BigDecimal.valueOf(10))
                        .build());
        when(accountTransactionRepository.create(any(DSLContext.class), eq(senderUUID), eq(receiverUUID), any(BigDecimal.class)))
                .thenReturn(returnedTransactionUUID);

        String transactionUUID = transferMoneyService.transfer(TransferOperation.of(SENDER_ACCOUNT_NUMBER, RECEIVER_ACCOUNT_NUMBER, BigDecimal.valueOf(100.11)));

        assertThat(transactionUUID, is(returnedTransactionUUID));
    }

    @Test(expected = TransferMoneyException.class)
    public void transferMoney_failed() {
        when(accountRepository.findByAccount(any(DSLContext.class), eq(SENDER_ACCOUNT_NUMBER)))
                .thenReturn(Account.builder()
                        .uuid(UUID.randomUUID().toString())
                        .account(SENDER_ACCOUNT_NUMBER)
                        .totalAmount(BigDecimal.valueOf(1000.11D))
                        .build());
        when(accountRepository.findByAccount(any(DSLContext.class), eq(RECEIVER_ACCOUNT_NUMBER)))
                .thenReturn(Account.builder()
                        .uuid(UUID.randomUUID().toString())
                        .account(RECEIVER_ACCOUNT_NUMBER)
                        .totalAmount(BigDecimal.valueOf(10D))
                        .build());

        doThrow(new TransferMoneyException(TransferStatus.FAIL, "Error transfer money"))
                .when(accountRepository)
                .findByAccount(any(DSLContext.class), any(String.class));

        transferMoneyService.transfer(TransferOperation.of(SENDER_ACCOUNT_NUMBER, RECEIVER_ACCOUNT_NUMBER, BigDecimal.valueOf(101.33)));
    }
}
