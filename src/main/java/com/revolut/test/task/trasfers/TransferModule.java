package com.revolut.test.task.trasfers;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.revolut.test.task.trasfers.repository.*;
import com.revolut.test.task.trasfers.service.TransferMoneyProcessor;
import com.revolut.test.task.trasfers.service.TransferMoneyService;
import com.revolut.test.task.trasfers.service.TransferMoneyServiceImpl;
import com.revolut.test.task.trasfers.support.DatabaseMigrationsExecutor;
import com.revolut.test.task.trasfers.support.SchedulerConfiguration;

public final class TransferModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Application.class).in(Singleton.class);
        bind(TransferMoneyProcessor.class).in(Singleton.class);
        bind(TransferMoneyService.class).to(TransferMoneyServiceImpl.class);
        bind(AccountRepository.class).to(AccountRepositoryImpl.class);
        bind(AccountTransactionRepository.class).to(AccountTransactionRepositoryImpl.class);
    }

    @Provides
    @Singleton
    @Inject
    public static DatabaseMigrationsExecutor databaseMigrationsExecutor(DatabaseConnectionContextHolder databaseConnectionContextHolder) {
        return new DatabaseMigrationsExecutor(databaseConnectionContextHolder.getConnectionConfiguration());
    }

    @Provides
    @Singleton
    public static DatabaseConnectionContextHolder databaseConnectionContextHolder() {
        return DatabaseConnectionContextHolder.init("application.properties");
    }

    @Provides
    @Singleton
    public static SchedulerConfiguration schedulerConfiguration() {
        return new SchedulerConfiguration(5, 5000);
    }
}
