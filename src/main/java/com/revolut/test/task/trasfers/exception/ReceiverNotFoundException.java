package com.revolut.test.task.trasfers.exception;

import static com.revolut.test.task.trasfers.beans.TransferStatus.RECEIVER_NOT_FOUND;

public class ReceiverNotFoundException extends AccountNotFoundException {
    public ReceiverNotFoundException(String account) {
        super(RECEIVER_NOT_FOUND, account);
    }
}
