package com.revolut.test.task.trasfers.repository;

import lombok.*;
import lombok.experimental.Tolerate;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.conf.Settings;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Properties;

import static java.util.Objects.requireNonNull;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class DatabaseConnectionContextHolder {

    private Configuration connectionConfiguration;
    private DataSource dataSource;

    private DataSource getDataSource() {
        if (dataSource == null) {
            throw new DataAccessException("Connection to database is not configured");
        }
        return dataSource;
    }

    @SneakyThrows
    public DSLContext getContext() {
        return DSL.using(
                getDataSource(),
                getConnectionConfiguration().getDialect(),
                new Settings().withExecuteWithOptimisticLocking(true)
        );
    }

    public Configuration getConnectionConfiguration() {
        if (connectionConfiguration == null) {
            throw new NullPointerException("Connection configuration is not set");
        }
        return connectionConfiguration;
    }

    @SneakyThrows
    private void safeCloseConnection() {
        if (dataSource != null) {
            ((BasicDataSource) dataSource).close();
        }
    }

    @SneakyThrows
    public void clearContext() {
        safeCloseConnection();
        connectionConfiguration = null;
        dataSource = null;
    }

    @SneakyThrows
    private static DatabaseConnectionContextHolder init(Configuration configuration) {
        DatabaseConnectionContextHolder contextHolder = new DatabaseConnectionContextHolder();
        contextHolder.connectionConfiguration = configuration;
        if (contextHolder.dataSource == null) {
            Class.forName(requireNonNull(configuration.getDriverName(), "Driver name is not set"));
            contextHolder.dataSource = setUpDataSource(configuration);
        }

        return contextHolder;
    }

    @SneakyThrows
    public static DatabaseConnectionContextHolder init(String fileName) {
        Properties properties = new Properties();
        try (InputStream is = DatabaseConnectionContextHolder.class.getClassLoader().getResourceAsStream(fileName)) {
            if (is == null) {
                throw new IllegalArgumentException("Cannot find property file with name: " + fileName);
            }
            properties.load(is);
            return init(Configuration.builder()
                    .url(properties.getProperty("db.url"))
                    .user(properties.getProperty("db.user"))
                    .password(properties.getProperty("db.password"))
                    .dialect(properties.getProperty("db.dialect"))
                    .driverName(properties.getProperty("db.driverName"))
                    .schema(properties.getProperty("db.schema"))
                    .build());
        }
    }

    private static DataSource setUpDataSource(Configuration configuration) {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(configuration.getUrl());
        dataSource.setDriverClassName(configuration.getDriverName());
        dataSource.setUsername(configuration.getUser());
        dataSource.setPassword(configuration.getPassword());
        dataSource.setDefaultTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        dataSource.setMaxIdle(configuration.getPool());
        dataSource.setDefaultQueryTimeout(30);
        return dataSource;
    }

    @Builder
    @Getter
    @AllArgsConstructor
    public static class Configuration {
        private static final int CONNECTION_POOL = 10;

        private final String url;
        private final String schema;
        private final String user;
        private final String password;
        private final String driverName;
        private final SQLDialect dialect;
        @Builder.Default
        private final int pool = CONNECTION_POOL;

        public static class ConfigurationBuilder {
            @Tolerate
            public ConfigurationBuilder dialect(String dialect) {
                requireNonNull(dialect);
                this.dialect = SQLDialect.valueOf(dialect.toUpperCase());
                return this;
            }
        }

    }
}
