package com.revolut.test.task.trasfers.exception;

import com.revolut.test.task.trasfers.beans.TransferStatus;
import lombok.Getter;

public class TransferMoneyException extends RuntimeException {

    @Getter private TransferStatus status;

    public TransferMoneyException(TransferStatus status, String message) {
        super(message, null, false, false);
        this.status = status;
    }
}
