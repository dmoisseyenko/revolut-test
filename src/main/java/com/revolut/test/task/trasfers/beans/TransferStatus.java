package com.revolut.test.task.trasfers.beans;

public enum TransferStatus {
    SUCCESSFUL,
    NOT_ENOUGH_MONEY,
    RECEIVER_NOT_FOUND,
    SENDER_NOT_FOUND,
    FAIL,
    SENDER_NOT_SET,
    RECEIVER_NOT_SET,
    AMOUNT_NOT_SET,
    NEGATIVE_OR_ZERO_AMOUNT,
    SAME_TRANSFER_ACCOUNT
}
