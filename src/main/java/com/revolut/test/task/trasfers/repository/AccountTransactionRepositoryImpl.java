package com.revolut.test.task.trasfers.repository;

import com.google.inject.Singleton;
import com.revolut.test.task.trasfers.beans.AccountTransactionStatus;
import lombok.val;
import org.jooq.DSLContext;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

import static com.revolut.test.task.tables.tables.AccountTransactions.ACCOUNT_TRANSACTIONS;

public class AccountTransactionRepositoryImpl implements AccountTransactionRepository {

    @Override
    public void updateStatus(DSLContext context, String uuid, AccountTransactionStatus status) {
        context.update(ACCOUNT_TRANSACTIONS)
                .set(ACCOUNT_TRANSACTIONS.MODIFICATION_DATETIME, LocalDateTime.now())
                .set(ACCOUNT_TRANSACTIONS.TRANSACTION_STATUS, status.name())
                .where(ACCOUNT_TRANSACTIONS.UUID.eq(uuid))
                .execute();
    }

    @Override
    public String create(DSLContext context, String senderUUID, String receiverUUID, BigDecimal amount) {
        val uuid = UUID.randomUUID().toString();
        val creationDatetime = LocalDateTime.now();
        context.insertInto(ACCOUNT_TRANSACTIONS)
                .columns(
                        ACCOUNT_TRANSACTIONS.UUID,
                        ACCOUNT_TRANSACTIONS.SENDER_UUID,
                        ACCOUNT_TRANSACTIONS.RECEIVER_UUID,
                        ACCOUNT_TRANSACTIONS.AMOUNT,
                        ACCOUNT_TRANSACTIONS.TRANSACTION_STATUS,
                        ACCOUNT_TRANSACTIONS.CREATION_DATETIME,
                        ACCOUNT_TRANSACTIONS.EXPIRE_DATETIME)
                .values(
                        uuid,
                        senderUUID,
                        receiverUUID,
                        amount,
                        AccountTransactionStatus.BLOCKED.name(),
                        creationDatetime,
                        creationDatetime.plusMonths(1)
                )
                .returning()
                .execute();
        return uuid;
    }
}
