package com.revolut.test.task.trasfers.support;

import com.revolut.test.task.trasfers.repository.DatabaseConnectionContextHolder;
import org.flywaydb.core.Flyway;

public final class DatabaseMigrationsExecutor {

    private final Flyway flyway;

    public DatabaseMigrationsExecutor(DatabaseConnectionContextHolder.Configuration configuration) {
        this.flyway = Flyway.configure()
                .dataSource(configuration.getUrl(), configuration.getUser(), configuration.getPassword())
                .schemas(configuration.getSchema())
                .load();
    }

    public void initOrUpdateSchema() {
        flyway.migrate();
    }

    public void clearSchema() {
        flyway.clean();
    }
}
