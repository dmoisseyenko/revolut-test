package com.revolut.test.task.trasfers.dto;

import com.revolut.test.task.trasfers.beans.TransferStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class TransferResponseDto {
    private String transactionUUID;
    private TransferStatus status;
    private String message;
}
