package com.revolut.test.task.trasfers.api;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.test.task.trasfers.beans.TransferOperation;
import com.revolut.test.task.trasfers.beans.TransferStatus;
import com.revolut.test.task.trasfers.dto.TransferDto;
import com.revolut.test.task.trasfers.dto.TransferResponseDto;
import com.revolut.test.task.trasfers.exception.TransferMoneyException;
import com.revolut.test.task.trasfers.exception.TransferMoneyValuesValidationException;
import com.revolut.test.task.trasfers.service.TransferMoneyProcessor;
import com.revolut.test.task.trasfers.service.TransferMoneyService;
import spark.Response;

import java.math.BigDecimal;

import static spark.Spark.*;

public class TransferMoneyRestApi {
    static final String TRANSFER_MONEY_ROUTE = "/api/transfer";

    private final TransferMoneyService transferMoneyService;
    private final TransferMoneyProcessor transferMoneyProcessor;

    @Inject
    public TransferMoneyRestApi(TransferMoneyService transferMoneyService, TransferMoneyProcessor transferMoneyProcessor) {
        this.transferMoneyService = transferMoneyService;
        this.transferMoneyProcessor = transferMoneyProcessor;
    }

    public void initRoutes() {
        transferMoneyProcessor.start();
        post(TRANSFER_MONEY_ROUTE, (req, resp) -> {
            TransferDto dto = new Gson().fromJson(req.body(), TransferDto.class);

            if (dto.getSender() == null) {
                throw new TransferMoneyValuesValidationException(TransferStatus.SENDER_NOT_SET, "Sender is not set");
            }

            if (dto.getReceiver() == null) {
                throw new TransferMoneyValuesValidationException(TransferStatus.RECEIVER_NOT_SET, "Receiver is not set");
            }

            if (dto.getAmount() == null) {
                throw new TransferMoneyValuesValidationException(TransferStatus.AMOUNT_NOT_SET, "Amount is not set");
            }

            String transactionUUID = transferMoneyService.transfer(TransferOperation.of(dto.getSender(), dto.getReceiver(), BigDecimal.valueOf(dto.getAmount())));
            return new Gson().toJson(TransferResponseDto.builder()
                    .status(TransferStatus.SUCCESSFUL)
                    .transactionUUID(transactionUUID)
                    .build());
        });

        after((req, resp) -> resp.type("application/json"));

        exception(TransferMoneyValuesValidationException.class, (ex, req, resp) -> errorResponse(resp, 400, ex));

        exception(TransferMoneyException.class, (ex, req, resp) -> errorResponse(resp, 500, ex));
    }

    private static void errorResponse(final Response resp, int httpCode, TransferMoneyException ex) {
        resp.status(httpCode);
        resp.body(new Gson().toJson(TransferResponseDto.builder()
                .status(ex.getStatus())
                .message(ex.getMessage())
                .build()));
    }
}
