package com.revolut.test.task.trasfers.repository;

import com.revolut.test.task.trasfers.beans.Account;
import org.jooq.DSLContext;

import java.math.BigDecimal;

public interface AccountRepository {
    Account findByAccount(DSLContext context, String id);

    void updateAmount(DSLContext context, String uuid, BigDecimal amount);
}
