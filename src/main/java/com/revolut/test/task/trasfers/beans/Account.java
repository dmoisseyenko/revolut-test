package com.revolut.test.task.trasfers.beans;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@Getter
@AllArgsConstructor
public class Account {
    private final String uuid;
    private final String account;
    private final BigDecimal totalAmount;
    private final BigDecimal availableAmount;
}
