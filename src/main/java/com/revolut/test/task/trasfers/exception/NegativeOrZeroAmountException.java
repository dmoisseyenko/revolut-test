package com.revolut.test.task.trasfers.exception;

import com.revolut.test.task.trasfers.beans.TransferStatus;

public class NegativeOrZeroAmountException extends TransferMoneyValuesValidationException {
    public NegativeOrZeroAmountException() {
        super(TransferStatus.NEGATIVE_OR_ZERO_AMOUNT, "Amount should be greater zero");
    }
}
