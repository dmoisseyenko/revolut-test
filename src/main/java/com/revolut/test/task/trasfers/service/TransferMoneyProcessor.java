package com.revolut.test.task.trasfers.service;

import com.google.inject.Inject;
import com.revolut.test.task.trasfers.beans.AccountTransaction;
import com.revolut.test.task.trasfers.beans.AccountTransactionStatus;
import com.revolut.test.task.trasfers.repository.AccountTransactionRepository;
import com.revolut.test.task.trasfers.repository.DatabaseConnectionContextHolder;
import com.revolut.test.task.trasfers.support.SchedulerConfiguration;
import lombok.extern.slf4j.Slf4j;
import lombok.val;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.revolut.test.task.tables.Tables.ACCOUNTS;
import static com.revolut.test.task.tables.tables.AccountTransactions.ACCOUNT_TRANSACTIONS;
import static com.revolut.test.task.trasfers.beans.AccountTransactionStatus.CANCELED;
import static com.revolut.test.task.trasfers.beans.AccountTransactionStatus.COMPLETED;
import static java.util.stream.Collectors.toSet;

@Slf4j
public class TransferMoneyProcessor implements Runnable {

    private final static int MAX_LIMIT_PROCESS_TRANSFERS_IN_ONE_TRANSACTION = 10;

    private final DatabaseConnectionContextHolder databaseConnectionContextHolder;
    private final SchedulerConfiguration schedulerConfiguration;
    private final AccountTransactionRepository accountTransactionRepository;

    private volatile Set<String> inProgress = ConcurrentHashMap.newKeySet();

    @Inject
    public TransferMoneyProcessor(DatabaseConnectionContextHolder databaseConnectionContextHolder,
            SchedulerConfiguration schedulerConfiguration,
            AccountTransactionRepository accountTransactionRepository
    ) {
        this.databaseConnectionContextHolder = databaseConnectionContextHolder;
        this.schedulerConfiguration = schedulerConfiguration;
        this.accountTransactionRepository = accountTransactionRepository;
    }

    @Override
    public void run() {
        try {
            if (inProgress.size() == MAX_LIMIT_PROCESS_TRANSFERS_IN_ONE_TRANSACTION) {
                log.warn("#run: Too many operations in process...");
                return;
            }
            log.debug("#run: start transfer operation");

            List<AccountTransaction> transferTransactions = databaseConnectionContextHolder.getContext()
                    .selectFrom(ACCOUNT_TRANSACTIONS)
                    .where(ACCOUNT_TRANSACTIONS.TRANSACTION_STATUS.eq(AccountTransactionStatus.BLOCKED.name()))
                    .andNot(ACCOUNT_TRANSACTIONS.UUID.in(inProgress))
                    .orderBy(ACCOUNT_TRANSACTIONS.CREATION_DATETIME.asc())
                    .limit(MAX_LIMIT_PROCESS_TRANSFERS_IN_ONE_TRANSACTION - inProgress.size())
                    .fetch()
                    .map(record -> AccountTransaction.builder()
                            .uuid(record.getUuid())
                            .senderUUID(record.getSenderUuid())
                            .receiverUUID(record.getReceiverUuid())
                            .amount(record.getAmount())
                            .build());

            log.debug("#run: found {} transfer operations", transferTransactions.size());

            if (!transferTransactions.isEmpty()) {
                inProgress.addAll(transferTransactions.stream().map(AccountTransaction::getUuid).collect(toSet()));
                transferTransactions.forEach(this::processTransfersTransaction);
            }
        } catch (Throwable ex) {
            log.error("#run: error execute transfer operation: " + ex.getMessage(), ex);
        }
    }

    private void processTransfersTransaction(AccountTransaction transaction) {
        log.debug("#processTransfersTransaction: start transfer transaction with uuid -> {}", transaction.getUuid());
        Objects.requireNonNull(transaction.getReceiverUUID());
        Objects.requireNonNull(transaction.getSenderUUID());
        Objects.requireNonNull(transaction.getAmount());
        try {
            databaseConnectionContextHolder.getContext().transaction(configuration -> {
                val context = configuration.dsl();
                context.update(ACCOUNTS)
                        .set(ACCOUNTS.TOTAL_AMOUNT, ACCOUNTS.TOTAL_AMOUNT.minus(transaction.getAmount()))
                        .where(ACCOUNTS.UUID.eq(transaction.getSenderUUID()))
                        .execute();

                context.update(ACCOUNTS)
                        .set(ACCOUNTS.TOTAL_AMOUNT, ACCOUNTS.TOTAL_AMOUNT.plus(transaction.getAmount()))
                        .where(ACCOUNTS.UUID.eq(transaction.getReceiverUUID()))
                        .execute();

                accountTransactionRepository.updateStatus(context, transaction.getUuid(), COMPLETED);
            });
        } catch (Throwable ex) {
            log.error("#processTransfersTransaction: error execute transaction: " + ex.getMessage(), ex);
            accountTransactionRepository.updateStatus(databaseConnectionContextHolder.getContext(), transaction.getUuid(), CANCELED);
        } finally {
            inProgress.remove(transaction.getUuid());
        }
        log.debug("#processTransfersTransaction: transfer transaction with uuid -> {} executed", transaction.getUuid());
    }

    public void start() {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(schedulerConfiguration.getTreadPoolSize());
        scheduledExecutorService.scheduleWithFixedDelay(this, 0, schedulerConfiguration.getPeriodicityInMs(), TimeUnit.MILLISECONDS);
        log.info("#start: transfer money scheduler started with configuration -> {}", schedulerConfiguration);
    }
}
