package com.revolut.test.task.trasfers.repository;

import com.revolut.test.task.trasfers.beans.Account;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.impl.DSL;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static com.revolut.test.task.tables.tables.AccountTransactions.ACCOUNT_TRANSACTIONS;
import static com.revolut.test.task.tables.tables.Accounts.ACCOUNTS;
import static java.util.stream.Collectors.toSet;

public class AccountRepositoryImpl implements AccountRepository {

    @Override
    public Account findByAccount(DSLContext context, String account) {
        Stream<Field<Object>> blockedSum = Stream.of(context.select(DSL.sum(ACCOUNT_TRANSACTIONS.AMOUNT))
                .from(ACCOUNT_TRANSACTIONS)
                .where(ACCOUNT_TRANSACTIONS.SENDER_UUID.eq(account))
                .asField("BLOCKED_SUM")
        );
        Set<Field<?>> fields = Stream.concat(Stream.of(ACCOUNTS.fields()), blockedSum).collect(toSet());

        return Optional.ofNullable(
                context.select(fields)
                        .from(ACCOUNTS)
                        .where(ACCOUNTS.ACCOUNT.eq(account))
                        .fetchOne())
                .map(o -> Account.builder()
                        .uuid(o.get(ACCOUNTS.UUID))
                        .account(o.get(ACCOUNTS.ACCOUNT))
                        .totalAmount(o.get(ACCOUNTS.TOTAL_AMOUNT))
                        .availableAmount(o.get(ACCOUNTS.TOTAL_AMOUNT)
                                .subtract(Optional.ofNullable(o.get("BLOCKED_SUM", BigDecimal.class))
                                        .orElse(BigDecimal.ZERO)))
                        .build()
                )
                .orElse(null);
    }

    @Override
    public void updateAmount(DSLContext context, String uuid, BigDecimal amount) {
        context.update(ACCOUNTS)
                .set(ACCOUNTS.TOTAL_AMOUNT, amount)
                .where(ACCOUNTS.UUID.eq(uuid))
                .execute();
    }
}
