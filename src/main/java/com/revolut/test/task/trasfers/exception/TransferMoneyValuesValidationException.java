package com.revolut.test.task.trasfers.exception;

import com.revolut.test.task.trasfers.beans.TransferStatus;

public class TransferMoneyValuesValidationException extends TransferMoneyException {
    public TransferMoneyValuesValidationException(TransferStatus status, String message) {
        super(status, message);
    }
}
