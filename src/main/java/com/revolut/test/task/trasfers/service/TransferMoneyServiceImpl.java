package com.revolut.test.task.trasfers.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.test.task.trasfers.beans.Account;
import com.revolut.test.task.trasfers.beans.TransferOperation;
import com.revolut.test.task.trasfers.beans.TransferStatus;
import com.revolut.test.task.trasfers.exception.*;
import com.revolut.test.task.trasfers.repository.AccountRepository;
import com.revolut.test.task.trasfers.repository.AccountTransactionRepository;
import com.revolut.test.task.trasfers.repository.DatabaseConnectionContextHolder;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.jooq.DSLContext;
import org.jooq.exception.DataAccessException;

import java.math.BigDecimal;

import static java.util.Objects.requireNonNull;

@Slf4j
public class TransferMoneyServiceImpl implements TransferMoneyService {

    private final AccountRepository accountRepository;
    private final AccountTransactionRepository accountTransactionRepository;
    private final DatabaseConnectionContextHolder databaseConnectionContextHolder;

    @Inject
    public TransferMoneyServiceImpl(AccountRepository accountRepository,
            DatabaseConnectionContextHolder databaseConnectionContextHolder,
            AccountTransactionRepository accountTransactionRepository) {
        this.accountRepository = accountRepository;
        this.databaseConnectionContextHolder = databaseConnectionContextHolder;
        this.accountTransactionRepository = accountTransactionRepository;
    }

    @Override
    public String transfer(TransferOperation transferOperation) throws TransferMoneyException {
        requireNonNull(transferOperation.getSender(), "Sender account is not set");
        requireNonNull(transferOperation.getReceiver(), "Receiver account is not set");

        log.debug("#transfer: start execute transer operation -> {}", transferOperation);

        if (transferOperation.getReceiver().equals(transferOperation.getSender())) {
            log.error("#transfer: error execute transfer, operation's account is the same");
            throw new SameTransferAccountException();
        }

        if (transferOperation.getAmount().compareTo(BigDecimal.ZERO) == 0 || transferOperation.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            log.error("#transfer: error execute transfer, transfer amount equals zero or less");
            throw new NegativeOrZeroAmountException();
        }

        try {
            return databaseConnectionContextHolder.getContext().transactionResult(c -> {
                DSLContext context = c.dsl();
                Account senderAccount = accountRepository.findByAccount(context, transferOperation.getSender());
                if (senderAccount == null) {
                    log.error("#transfer: not found sender with account: {}", transferOperation.getSender());
                    throw new SenderNotFoundException(transferOperation.getSender());
                }

                log.debug("#transfer: found account: {} like a sender", senderAccount.getAccount());

                Account receiverAccount = accountRepository.findByAccount(context, transferOperation.getReceiver());
                if (receiverAccount == null) {
                    log.error("#transfer: not found receiver with account: {}", transferOperation.getReceiver());
                    throw new ReceiverNotFoundException(transferOperation.getReceiver());
                }

                log.debug("#transfer: found account: {} like a receiver", receiverAccount.getAccount());

                if (senderAccount.getAvailableAmount().compareTo(transferOperation.getAmount()) < 0) {
                    log.error("#transfer: sender {} have not enough money", senderAccount.getAccount());
                    throw new NotEnoughMoneyException(transferOperation.getSender(), senderAccount.getTotalAmount());
                }

                val transactionUUID = accountTransactionRepository.create(context, senderAccount.getUuid(), receiverAccount.getUuid(), transferOperation.getAmount());
                log.debug("#transfer: sender {} transfer operation to {} on {} added in queue with UUID -> {}"
                        , senderAccount.getAccount(), receiverAccount.getAccount(), transferOperation.getAmount(), transactionUUID);
                return transactionUUID;
            });
        } catch (DataAccessException ex) {
            throw new TransferMoneyException(TransferStatus.FAIL, ex.getMessage());
        }
    }


}
