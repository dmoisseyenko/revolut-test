package com.revolut.test.task.trasfers.service;

import com.revolut.test.task.trasfers.beans.TransferOperation;
import com.revolut.test.task.trasfers.beans.TransferStatus;
import com.revolut.test.task.trasfers.exception.TransferMoneyException;

public interface TransferMoneyService {

    String transfer(TransferOperation transferOperation) throws TransferMoneyException;
}
