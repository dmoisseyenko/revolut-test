package com.revolut.test.task.trasfers.beans;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@AllArgsConstructor(staticName = "of")
public class TransferOperation {
    @Getter private final String sender;
    @Getter private final String receiver;
    @Getter private final BigDecimal amount;
}
