package com.revolut.test.task.trasfers.exception;

import static com.revolut.test.task.trasfers.beans.TransferStatus.SENDER_NOT_FOUND;

public class SenderNotFoundException extends AccountNotFoundException {
    public SenderNotFoundException(String account) {
        super(SENDER_NOT_FOUND, account);
    }
}
