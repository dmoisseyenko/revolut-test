package com.revolut.test.task.trasfers.exception;

import java.math.BigDecimal;

import static com.revolut.test.task.trasfers.beans.TransferStatus.NOT_ENOUGH_MONEY;

public class NotEnoughMoneyException extends TransferMoneyValuesValidationException {

    public NotEnoughMoneyException(String account, BigDecimal amount) {
        super(NOT_ENOUGH_MONEY, String.format("Not enough money on %s account. Account have only %s", account, amount));
    }
}
