package com.revolut.test.task.trasfers.support;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class SchedulerConfiguration {
    private final int treadPoolSize;
    private final int periodicityInMs;
}
