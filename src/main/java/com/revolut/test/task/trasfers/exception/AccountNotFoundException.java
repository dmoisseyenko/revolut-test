package com.revolut.test.task.trasfers.exception;

import com.revolut.test.task.trasfers.beans.TransferStatus;

class AccountNotFoundException extends TransferMoneyValuesValidationException {

    AccountNotFoundException(TransferStatus status, String account) {
        super(status, String.format("Account %s is not found", account));
    }
}
