package com.revolut.test.task.trasfers.support;

import com.revolut.test.task.trasfers.beans.AccountTransactionStatus;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jooq.impl.EnumConverter;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class EnumConverters {
    public static AccountTransactionStatusConverter accountTransferConverter() {
        return new AccountTransactionStatusConverter();
    }

    public static class AccountTransactionStatusConverter extends EnumConverter<String, AccountTransactionStatus> {
        public AccountTransactionStatusConverter() {
            super(String.class, AccountTransactionStatus.class);
        }
    }
}

