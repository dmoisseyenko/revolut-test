package com.revolut.test.task.trasfers;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.revolut.test.task.trasfers.api.TransferMoneyRestApi;
import com.revolut.test.task.trasfers.support.DatabaseMigrationsExecutor;

public class Application{

    @Inject
    public Application(TransferMoneyRestApi transferMoneyRestApi, DatabaseMigrationsExecutor databaseMigrationsExecutor) {
        databaseMigrationsExecutor.initOrUpdateSchema();
        transferMoneyRestApi.initRoutes();
    }

    public static void main(String[] args) {
        Guice.createInjector(new TransferModule()).getInstance(Application.class);
    }
}
