package com.revolut.test.task.trasfers.repository;

import com.revolut.test.task.trasfers.beans.AccountTransactionStatus;
import org.jooq.DSLContext;

import java.math.BigDecimal;

public interface AccountTransactionRepository {
    void updateStatus(DSLContext context, String uuid, AccountTransactionStatus status);

    String create(DSLContext context, String senderUUID, String receiverUUID, BigDecimal amount);
}
