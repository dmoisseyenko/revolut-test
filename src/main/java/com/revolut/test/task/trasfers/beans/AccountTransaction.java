package com.revolut.test.task.trasfers.beans;

import lombok.Builder;
import lombok.Getter;

import java.math.BigDecimal;

@Builder
@Getter
public class AccountTransaction {
    private final String uuid;
    private final String senderUUID;
    private final String receiverUUID;
    private final BigDecimal amount;
}
