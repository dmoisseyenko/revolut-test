package com.revolut.test.task.trasfers.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TransferDto {
    private String sender;
    private String receiver;
    private Double amount;
}
