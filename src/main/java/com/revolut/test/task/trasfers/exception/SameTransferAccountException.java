package com.revolut.test.task.trasfers.exception;

import static com.revolut.test.task.trasfers.beans.TransferStatus.SAME_TRANSFER_ACCOUNT;

public class SameTransferAccountException extends TransferMoneyValuesValidationException {
    public SameTransferAccountException() {
        super(SAME_TRANSFER_ACCOUNT, "Cannot transfer money on the same account");
    }
}
