package com.revolut.test.task.trasfers.beans;

public enum AccountTransactionStatus {
    BLOCKED, COMPLETED, CANCELED
}
