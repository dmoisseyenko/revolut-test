#### Description
Revolut test task for transfer money for Senior Java Developer position

#### Used libraries
For implementation task are used libraries

1. [Spark framework](http://sparkjava.com/) - micro framework for creating web applications 
2. [JOOQ](https://www.jooq.org/) - for work with database 
3. [FlyWay](https://flywaydb.org/) - for initialization db schema and execution of migration scripts 
(for example: create tables, add test data and etc.)
4. [H2](http://www.h2database.com/html/main.html) - simple database.
It's used like in-memory database for tests, for generation of meta classes for jooq.

#### Start application
For start application you need to execute gradle task `gradlew runApp` or you can build application by 
gradle tasks `gradlew clean build` and run jar file `java -jar build/libs/revolut-test.jar`.